---
title: "Daily Wrap-Up: 11 Jan 2018"
date: 2018-01-11T19:25:46-05:00
draft: false
type: "post"
description: "It's been a pretty rough first couple weeks of the year, illness took me out of the picture for a while. But I'm back. And the content will flow."
image: "img/21_final_fantasy_xiv.jpg"
tags: ["ffxiv", "twitch"]
categories: ["dailies"]
---
It's been a pretty rough first couple weeks of the year. I was quite hyped, as
the year started coming to an end, to tackle things full-force; however, an illness
that has been going around took me out of commission for a couple weeks, and
I hate not publishing or producing.

That said, I've started on the new schedule of production, starting with the daily
Twitch streams over at [Twitch.tv/sevvieRose](https://twitch.tv/sevvierose) which
start at 11am EST (4pm GMT) most weekdays. I won't say "every weekday" because I
know me. I know there will be a day or two every so often where I'm just not well
enough to hop in front of the camera, or grind through games.

That said, there's not much content to share from today other than the one Twitch
VOD; so... enjoy!

* [FFXIV Dailies on Twitch](https://www.twitch.tv/videos/217538604)

And for tomorrow, you have blockchain/cryptocurrency discussions to look forward to!
I'll see you then.
