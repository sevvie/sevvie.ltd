---
title: "Daily Wrap-Up: 12 Jan 2018"
date: 2018-01-12T20:20:58-05:00
draft: false
type: "post"
description: "After last night's update, my beastly rendering/streaming machine decided to lock up on restart again, and refused to boot after that. Luckily, I had it fixed before today's Twitch show..."
image: 'img/burning-computer.png'
tags: ['windows', 'linux', 'ffxiv']
categories: ['dailies']
---

(No, the picture above is NOT my computer.)

After updating the website last night, everything went to shit. And by everything,
I mean my beast of a desktop computer which, regrettably, runs Windows for
game-streaming and video-editing, shat itself for the first time of 2018. It had
been locking up on shutdown/restart every so often in Decembre, and I presumed
it was a hard drive dying -- something I can't afford to replace at the moment --
however, it has been pretty stable since Christmas. So instead of sleeping last
night, I was doing backups, reinstalling Windows and all the applications I use,
and (perhaps the most important part) restoring all my keyboard and UI tweaks to
make it my own again.

I'm not sure if you all are interested in a discussion of the Windows Linux Subsystem
and editing dotfiles, so I'll save you the rant for now -- however, if you'd like
me to do a write-up on how I set up Windows for both streaming and development, just
ask! I think I'd enjoy producing some content on those subjects.

With that said, I only had finished installing the essentials for streaming and
set up scenes in OBS like 20min before today's Twitch stream -- cutting it quite
close. But all went well -- and I even got my first two subscribers on Twitch! It
was quite exciting honestly.

Next up for me is a lot of preparation for tomorrow night's 7 @ 7 show, which is
looking to be quite dense with content -- we're gonna have to cover both the real
news of the week and the forced drama used by media networks to ignore the real
news, as it kinda ties together this time. Normally, I'd avoid a story about Trump
calling Haiti a "shithole country", but unfortunately no one else can -- not even
Google or Twitter, two of the main subjects of tomorrow's show.

But no more spoilers! Here's the links from today:

* : [FFXIV Questing and Blockchain Musing](https://www.twitch.tv/videos/217842131)

Catch you tomorrow for 7 @ 7!
