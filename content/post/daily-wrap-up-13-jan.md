---
title: "Daily Wrap-Up 13 Jan 2018"
date: 2018-01-13T21:53:47-05:00
draft: false
type: "post"
description: "I'm really proud of the work I do on 7 @ 7, my weekly news digest stream on YouTube. This week, I covered both the 'shithole countries' stories, as well as stories that matter like #AmericanPravda and James Damore's Lawsuit."
image: "img/silicon-valley.jpg"
tags: ['7@7', 'google', 'twitter', 'censorship', 'shithole']
categories: ['dailies', '7 @ 7']
---

Another week, another 7 @ 7.

I'm really proud of the work I do on 7 @ 7, my weekly news digest stream on
YouTube. This week, I covered both the 'shithole countries' stories, as well as
stories that matter like #AmericanPravda and James Damore's Lawsuit. The
censorship and outright hypocrisy exercised by companies like Twitter and YouTube
are a sensitive subject for me, one on which I am very passionate; and this week
would have been a Christmas-come-Late had the major media and internet outlets
not done all in their power to cover up these important stories -- but that, too,
is a story worth covering.

When it comes to doing these little Daily Wrap-ups, I normally have just the one
video to link, but for Saturdays I'll be posting a much more comprehensive list
of sources used to put together the show; it's the same list from the description
box of the video, though I think having all my sources here as well will help
in spreading those links (and their archive.is counterparts).

I hope you enjoy the stream, and I'll catch y'all on Monday.

- [7 @ 7, 13 Jan 2018](https://www.youtube.com/watch?v=J_U8uh-7lGw)

#### Chris Matthews asks for his Bill Cosby Pill:
- https://www.aol.com/article/entertainment/2018/01/12/chris-matthews-asks-for-his-bill-cosby-pill-before-hillary-clinton-interview/23332263/ ( https://archive.is/VCiMb )

#### Trump and the Shithole Countries:
- https://www.washingtonpost.com/politics/trump-attacks-protections-for-immigrants-from-shithole-countries-in-oval-office-meeting/2018/01/11/bfc0725c-f711-11e7-91af-31ac729add94_story.html?utm_term=.dc316524fb28 ( https://archive.is/Bchqi )
- https://video.foxnews.com/v/5708320913001/?#sp=show-clips (video) 
- https://news.grabien.com/story-media-meltdown-over-trumps-s-word-nazi-evil-kkk-terrorist-sy ( https://archive.is/oWuQe )

#### Treasury Holdings Scare:
- https://www.bloomberg.com/news/articles/2018-01-10/china-officials-are-said-to-view-treasuries-as-less-attractive ( https://archive.is/t7FOU )
- http://ticdata.treasury.gov/Publish/mfh.txt (Text File) 
- http://www.atimes.com/article/fake-news-bloomberg-chinas-us-treasury-holdings/  ( https://archive.is/uDAhM )

#### Twitter's Social Justice Campaign:
- http://www.breitbart.com/tech/2018/01/12/twitter-leaker-hollywood-celebs-hate-speech-violence/ ( https://archive.is/LbVfO )
- https://www.projectveritas.com/2018/01/11/undercover-video-twitter-engineers-to-ban-a-way-of-talking-through-shadow-banning-algorithms-to-censor-opposing-political-opinions/ (video) 
- https://www.projectveritas.com/2018/01/12/brent-bozell-writes-public-letter-to-twitters-jack-dorsey-demanding-a-response-in-wake-of-hidden-camera-twitter-expose/ ( https://archive.is/wOzZ8 ) 
- https://www.dangerous.com/40137/milo-roger-stone-announce-lawsuit-twitter/ ( https://archive.is/LRJ7x ) 

#### James Damore's Class-Action Lawsuit Against Google:
- https://techcrunch.com/2018/01/08/james-damore-just-filed-a-class-action-lawsuit-against-google-saying-it-discriminates-against-white-male-conservatives/ ( https://archive.is/uQ5Ms )
- http://www.breitbart.com/tech/2018/01/10/damore-lawsuit-claims-google-let-employee-who-identifies-as-yellow-scaled-wingless-dragonkin-give-presentation/ ( https://archive.is/U73SX )
- https://www.theverge.com/2018/1/12/16885560/david-gudeman-james-damore-google-lawsuit-misandrist-liberal-hate-group-accusation ( https://archive.is/p8H1W )
- https://www.scribd.com/document/368692388/James-Damore-Lawsuit (PDF) 

### MUSIC & MEMES

- Bulgarian Man Gives Putin a Dog: https://www.youtube.com/watch?v=pQzzon1CwMA
- Snowy Pines (Terra Theme OCRemix) by RebeccaETripp: https://www.youtube.com/watch?v=LDx8N5tnkRQ
- Addicted to Stress (Orchestral Cover) by Nathan Hanover: https://www.youtube.com/watch?v=aH4M84V8T0M
