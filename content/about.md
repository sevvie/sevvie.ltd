---
title: "About"
date: 2018-01-11T20:32:16-05:00
draft: false
type: "page"
comments: false
noauthor: true
share: false
---

You want to know more? Cool.

I'm all over the internet, taking advantage of any service I can to spread my
published writings and videos, as well as just to start conversations. I'd love
it if you joined me on your favourite ones!

* https://gab.ai/sevvie
* https://minds.com/sevvie
* https://twitter.com/sevvierose
* https://youtube.com/c/sevvieRoseTV
* https://bitchute.com/channel/sevvie
* https://patreon.com/sevvie
* https://makersupport.com/sevvie
* https://streamlabs.com/sevvierose
